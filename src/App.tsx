import ListGroup from "./components/ListGroup";

function App() {
  const items = ["New York", "San Franscisco", "Tokyo", "London", "Berlin"];
  const handleSelection = (item: string) => {console.log(item);}
  return (
    <div>
      <ListGroup items={items} heading="Cities"  onSelectItem={handleSelection}/>
    </div>
  );
}

export default App;
