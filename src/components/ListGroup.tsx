// import { Fragment } from "react";
// import { MouseEvent } from "react";

import { useState } from "react";

interface ListGroupProp {
    items: string[];
    heading: string;
    onSelectItem: (item: string) => void;
    
}

function ListGroup({ items, heading, onSelectItem }: ListGroupProp) {

  // EVENT HANDLER
  //const handleClick = (event: MouseEvent) => console.log(event);
  //const getMessage = () => {items.length == 0 ?  <p>No item found</p> : null}
  const getMessage2 = () => {
    items.length == 0 && <p>No item found</p>;
  };

  const [selectedIndex, setSelectedIndex] = useState(-1);
//   const [name, setName] = useState('');
  

  return (
    <>
      <h1>{heading}</h1>
      {getMessage2()}
      <ul className="list-group">
        {items.map((item, index) => (
          <li
            className={
              selectedIndex === index
                ? "list-group-item active"
                : "list-group-item"
            }
            key={item}
            onClick={() => {
              setSelectedIndex(index);
              onSelectItem(item);
            }}
          >
            {item}
          </li>
        ))}
      </ul>
    </>
  );
}

export default ListGroup;
